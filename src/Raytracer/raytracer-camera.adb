package body Raytracer.Camera is

   function Create return C_Camera is
   begin
      return (Origin => (0.0, 0.0, 0.0),
              Lower_Left_Corner => (-2.0, -1.0, -1.0),
              Horizontal => (4.0, 0.0, 0.0),
              Vertical => (0.0, 2.0, 0.0));
   end Create;

   -------------------------------------------------------------------------------------------------

   function Get_Ray (camera : in C_Camera; u, v : in DT_Float) return C_Ray is
   begin
      return Raytracer.Rays.Make_Ray (origin => camera.Origin,
                                      direction => camera.Lower_Left_Corner +
                                      u * camera.Horizontal + v * camera.Vertical - camera.Origin);
   end Get_Ray;

end Raytracer.Camera;
