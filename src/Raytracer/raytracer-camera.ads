with Raytracer.Vectors; use Raytracer.Vectors;
with Raytracer.Rays; use Raytracer.Rays;

package Raytracer.Camera is

   type C_Camera is tagged record
      Origin            : XC_Vector3F;
      Lower_Left_Corner : XC_Vector3F;
      Horizontal        : XC_Vector3F;
      Vertical          : XC_Vector3F;
   end record;

   function Create return C_Camera;
   function Get_Ray (camera : in C_Camera; u, v : in DT_Float) return C_Ray;

end Raytracer.Camera;
