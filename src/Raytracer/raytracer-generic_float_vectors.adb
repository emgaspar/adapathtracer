package body Raytracer.Generic_Float_Vectors is

   function "*"(Left, Right : in XC_Vector) return XC_Vector is
      Result : XC_Vector;
   begin
      for idx in DT_Index_Type'Range loop
         Result (idx) := Left (idx) * Right (idx);
      end loop;
      return Result;
   end "*";

   ---------------------------------------------------------------------------------------------------------------------

   function "*"(Left : in DT_Element_Type; Right : in XC_Vector) return XC_Vector is
      Result : XC_Vector;
   begin
      for idx in DT_Index_Type'Range loop
         Result (idx) := Left * Right (idx);
      end loop;
      return Result;
   end "*";

   ---------------------------------------------------------------------------------------------------------------------

   function "*"(Left : in XC_Vector; Right : in DT_Element_Type) return XC_Vector is
      Result : XC_Vector;
   begin
      for idx in DT_Index_Type'Range loop
         Result (idx) := Left (idx) * Right;
      end loop;
      return Result;
   end "*";

   ---------------------------------------------------------------------------------------------------------------------

   function "+"(Left, Right : in XC_Vector) return XC_Vector is
      Result : XC_Vector;
   begin
      for idx in DT_Index_Type'Range loop
         Result (idx) := Left (idx) + Right (idx);
      end loop;
      return Result;
   end "+";

   ---------------------------------------------------------------------------------------------------------------------

   function "-"(Left, Right : in XC_Vector) return XC_Vector is
      Result : XC_Vector;
   begin
      for idx in DT_Index_Type'Range loop
         Result (idx) := Left (idx) - Right (idx);
      end loop;
      return Result;
   end "-";

   ---------------------------------------------------------------------------------------------------------------------

   function "-"(Vector : in XC_Vector) return XC_Vector is
      Result : XC_Vector;
   begin
      for idx in DT_Index_Type'Range loop
         Result (idx) := -Vector (idx);
      end loop;
      return Result;
   end "-";

   ---------------------------------------------------------------------------------------------------------------------

   function "/"(Left, Right : in XC_Vector) return XC_Vector is
      Result : XC_Vector;
   begin
      for idx in DT_Index_Type'Range loop
         Result (idx) := Left (idx) / Right (idx);
      end loop;
      return Result;

   end "/";

   ---------------------------------------------------------------------------------------------------------------------

   function "/"(Left : in XC_Vector; Right : in DT_Element_Type) return XC_Vector is
      Result : XC_Vector;
   begin
      for idx in DT_Index_Type'Range loop
         Result (idx) := Left (idx) / Right;
      end loop;
      return Result;
   end "/";

   ---------------------------------------------------------------------------------------------------------------------

   function Dot_Product (Left, Right : in XC_Vector) return DT_Element_Type is
      Result : DT_Element_Type := DT_Element_Type (0.0);
   begin
      for idx in DT_Index_Type'Range loop
         Result := Result + (Left (idx) * Right (idx));
      end loop;
      return Result;
   end Dot_Product;

   ---------------------------------------------------------------------------------------------------------------------

   function Identity return XC_Vector is
      Vector : XC_Vector;
   begin
      for idx in DT_Index_Type'Range loop
         Vector (idx) := DT_Element_Type (1.0);
      end loop;
      return Vector;
   end Identity;

   ---------------------------------------------------------------------------------------------------------------------

   function Length (Vector : in XC_Vector) return DT_Element_Type is
   begin
      return Elementary_Functions.Sqrt (Squared_Length (Vector));
   end Length;

   function Normalize (Vector : in XC_Vector) return XC_Vector is
   begin
      return Vector / Length (Vector);
   end Normalize;

   ---------------------------------------------------------------------------------------------------------------------

   function Reflect (Left, Right : in XC_Vector) return XC_Vector is
   begin
      return Left - (2.0 * Dot_Product (Left, Right) * Right);
   end Reflect;

   procedure Refract (v, n         : in XC_Vector;
                      ni_over_nt   : in DT_Element_Type;
                      refracted    : out XC_Vector;
                      is_refracted : out Boolean) is

      uv           : XC_Vector;
      dt           : DT_Element_Type;
      discriminant : DT_Element_Type;

   begin

      uv := Normalize (v);
      dt := Dot_Product (uv, n);
      discriminant := DT_Element_Type (1.0) - ni_over_nt * ni_over_nt * (DT_Element_Type (1.0) - dt * dt);

      if discriminant > DT_Element_Type (0.0) then
         refracted := ni_over_nt * (v - n * dt) - n * Sqrt (discriminant);
         is_refracted := True;
      else
         is_refracted := False;
      end if;

   end Refract;

   ---------------------------------------------------------------------------------------------------------------------

   function Squared_Length (Vector : in XC_Vector) return DT_Element_Type is
      Len : DT_Element_Type := DT_Element_Type (0.0);
   begin
      for idx in DT_Index_Type'Range loop
         Len := Len + (Vector (idx) * Vector (idx));
      end loop;
      return Len;
   end Squared_Length;

   ---------------------------------------------------------------------------------------------------------------------

   function Zeros return XC_Vector is
      Vector : XC_Vector;
   begin
      for idx in DT_Index_Type'Range loop
         Vector (idx) := DT_Element_Type (0.0);
      end loop;
      return Vector;
   end Zeros;

end Raytracer.Generic_Float_Vectors;
