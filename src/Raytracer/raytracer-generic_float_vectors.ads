with Ada.Numerics.Generic_Elementary_Functions;
use Ada.Numerics;

generic
   type DT_Index_Type is (<>);
   type DT_Element_Type is digits <>;
package Raytracer.Generic_Float_Vectors is

   type XC_Vector is array (DT_Index_Type) of aliased DT_Element_Type;

   function Identity return XC_Vector;
   function Zeros return XC_Vector;

   function "+"(Left, Right : in XC_Vector) return XC_Vector;
   function "-"(Left, Right : in XC_Vector) return XC_Vector;

   function "-"(Vector : in XC_Vector) return XC_Vector;

   function "*"(Left, Right : in XC_Vector) return XC_Vector;
   function "*"(Left : in DT_Element_Type; Right : in XC_Vector) return XC_Vector;
   function "*"(Left : in XC_Vector; Right : in DT_Element_Type) return XC_Vector;

   function "/"(Left, Right : in XC_Vector) return XC_Vector;
   function "/"(Left : in XC_Vector; Right : in DT_Element_Type) return XC_Vector;

   function Length (Vector : in XC_Vector) return DT_Element_Type;
   function Squared_Length (Vector : in XC_Vector) return DT_Element_Type;

   function Normalize (Vector : in XC_Vector) return XC_Vector;

   function Dot_Product (Left, Right : in XC_Vector) return DT_Element_Type;

   function Reflect (Left, Right : in XC_Vector) return XC_Vector;

   procedure Refract (v, n         : in XC_Vector;
                      ni_over_nt   : in DT_Element_Type;
                      refracted    : out XC_Vector;
                      is_refracted : out Boolean);

   pragma Inline ("+");
   pragma Inline ("-");
   pragma Inline ("*");
   pragma Inline ("/");

private
   package Elementary_Functions is
     new Ada.Numerics.Generic_Elementary_Functions (DT_Element_Type);
   use Elementary_Functions;

end Raytracer.Generic_Float_Vectors;
