with Raytracer.Random_Generator; use Raytracer.Random_Generator;

package body Raytracer.Materials.Dielectric is

   function Make_Dielectric (ri : in DT_Float) return C_Dielectric is
   begin
      return (Refraction_Index => ri);
   end Make_Dielectric;

   -------------------------------------------------------------------------------------------------

   function Make_DielectricPtr (ri : in DT_Float) return C_MaterialPtr is
      material : constant C_DielectricPtr := new C_Dielectric;
   begin
      material.all := Make_Dielectric (ri => ri);
      return C_MaterialPtr (material);
   end Make_DielectricPtr;

   -------------------------------------------------------------------------------------------------

   overriding function Scatter (dielectric    : in C_Dielectric;
                                ray_in        : in C_Ray;
                                hit_record    : in Raytracer.Shapes.XC_Hit_Record;
                                attenuation   : out XC_Vector3F;
                                ray_scattered : out C_Ray) return Boolean is

      outward_normal : XC_Vector3F;
      reflected      : XC_Vector3F;
      refracted      : XC_Vector3F;
      is_refracted   : Boolean;
      ni_over_nt     : DT_Float;
      reflect_prob   : DT_Float;
      cosine         : DT_Float;

   begin
      reflected := Reflect (ray_in.Direction, hit_record.normal);
      attenuation := Identity;

      if Dot_Product (ray_in.Direction, hit_record.normal) > 0.0 then
         outward_normal := -hit_record.normal;
         ni_over_nt := dielectric.Refraction_Index;
         cosine := Dot_Product (ray_in.Direction, hit_record.normal) / Length (ray_in.Direction);

         cosine := 1.0 -
           dielectric.Refraction_Index * dielectric.Refraction_Index * (1.0 - cosine * cosine);

         -- Se añade una protección para evitar la raíz de un número negativo
         if cosine >= 0.0 then
            cosine := Sqrt (cosine);
         else
            cosine := 0.0;
         end if;
      else
         outward_normal := hit_record.normal;
         ni_over_nt := 1.0 / dielectric.Refraction_Index;
         cosine := -Dot_Product (ray_in.Direction, hit_record.normal) / Length (ray_in.Direction);
      end if;

      Refract (v            => ray_in.Direction,
               n            => outward_normal,
               ni_over_nt   => ni_over_nt,
               refracted    => refracted,
               is_refracted => is_refracted);

      if is_refracted = True then
         reflect_prob := Schlick (cosine           => cosine,
                                  refraction_index => dielectric.Refraction_Index);
      else
         reflect_prob  := 1.0;
      end if;

      if Get_Random < reflect_prob then
         ray_scattered := Make_Ray (origin => hit_record.point, direction => reflected);
      else
         ray_scattered := Make_Ray (origin => hit_record.point, direction => refracted);
      end if;

      return True;

   end Scatter;

   -------------------------------------------------------------------------------------------------

   function Schlick (cosine : in DT_Float; refraction_index : in DT_Float) return DT_Float is

      r0 : DT_Float;

   begin
      r0 := (1.0 - refraction_index) / (1.0 + refraction_index);
      r0 := r0 * r0;

      if (1.0 - cosine) >= 1.0e-6 then
         return r0 + (1.0 - r0) * ((1.0 - cosine)**5.0);
      else
         return r0;
      end if;

   end Schlick;

end Raytracer.Materials.Dielectric;
