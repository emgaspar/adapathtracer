with Raytracer.Materials;
with Raytracer.Vectors;
with Raytracer.Shapes;

package Raytracer.Materials.Dielectric is

   type C_Dielectric is new Raytracer.Materials.C_Material with private;
   type C_DielectricPtr is access all C_Dielectric;

   function Make_Dielectric (ri : in DT_Float) return C_Dielectric;
   function Make_DielectricPtr (ri : in DT_Float) return C_MaterialPtr;

   overriding function Scatter (dielectric    : in C_Dielectric;
                                ray_in        : in C_Ray;
                                hit_record    : in Raytracer.Shapes.XC_Hit_Record;
                                attenuation   : out XC_Vector3F;
                                ray_scattered : out C_Ray) return Boolean;

private
   type C_Dielectric is new Raytracer.Materials.C_Material with record
      Refraction_Index : DT_Float;
   end record;

   function Schlick (cosine : in DT_Float; refraction_index : in DT_Float) return DT_Float;

end Raytracer.Materials.Dielectric;
