package body Raytracer.Materials.Lambertian is

   function Make_Lambertian (albedo : in XC_Vector3F) return C_Lambertian is
   begin
      return (Albedo => albedo);
   end Make_Lambertian;

   -------------------------------------------------------------------------------------------------

   function Make_LambertianPtr (albedo : in XC_Vector3F) return C_MaterialPtr is
      material : constant C_LambertianPtr := new C_Lambertian;
   begin
      material.all := Make_Lambertian (albedo => albedo);
      return C_MaterialPtr (material);
   end Make_LambertianPtr;

   -------------------------------------------------------------------------------------------------

   overriding function Scatter (lambertian    : in C_Lambertian;
                                ray_in        : in C_Ray;
                                hit_record    : in Raytracer.Shapes.XC_Hit_Record;
                                attenuation   : out XC_Vector3F;
                                ray_scattered : out C_Ray) return Boolean is

      target : XC_Vector3F;

   begin
      target := hit_record.point + hit_record.normal + Materials.Random_In_Unit_Sphere;
      ray_scattered := Make_Ray (hit_record.point, target - hit_record.point);
      attenuation := lambertian.Albedo;
      return True;
   end Scatter;

end Raytracer.Materials.Lambertian;
