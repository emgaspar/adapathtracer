with Raytracer.Materials;
with Raytracer.Vectors;
with Raytracer.Shapes;

package Raytracer.Materials.Lambertian is

   type C_Lambertian is new Raytracer.Materials.C_Material with private;
   type C_LambertianPtr is access all C_Lambertian;

   function Make_Lambertian (albedo : in XC_Vector3F) return C_Lambertian;
   function Make_LambertianPtr (albedo : in XC_Vector3F) return C_MaterialPtr;

   overriding function Scatter (lambertian    : in C_Lambertian;
                                ray_in        : in C_Ray;
                                hit_record    : in Raytracer.Shapes.XC_Hit_Record;
                                attenuation   : out XC_Vector3F;
                                ray_scattered : out C_Ray) return Boolean;

private
   type C_Lambertian is new Raytracer.Materials.C_Material with record
      Albedo : XC_Vector3F;
   end record;

end Raytracer.Materials.Lambertian;
