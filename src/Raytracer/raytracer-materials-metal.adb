package body Raytracer.Materials.Metal is

   function Make_Metal (albedo : in XC_Vector3F) return C_Metal is
   begin
      return (Albedo => albedo);
   end Make_Metal;

   -------------------------------------------------------------------------------------------------

   function Make_MetalPtr (albedo : in XC_Vector3F) return C_MaterialPtr is
      material : constant C_MetalPtr := new C_Metal;
   begin
      material.all := Make_Metal (albedo => albedo);
      return C_MaterialPtr (material);
   end Make_MetalPtr;

   -------------------------------------------------------------------------------------------------

   overriding function Scatter (metal         : in C_Metal;
                                ray_in        : in C_Ray;
                                hit_record    : in Raytracer.Shapes.XC_Hit_Record;
                                attenuation   : out XC_Vector3F;
                                ray_scattered : out C_Ray) return Boolean is

      reflected : XC_Vector3F;

   begin
      reflected := Reflect (Left => Normalize (ray_in.Direction),
                            Right => hit_record.normal);
      ray_scattered := Make_Ray (hit_record.point, reflected);
      attenuation := metal.Albedo;
      return (Dot_Product (ray_scattered.Direction, hit_record.normal) > 0.0);
   end Scatter;

end Raytracer.Materials.Metal;
