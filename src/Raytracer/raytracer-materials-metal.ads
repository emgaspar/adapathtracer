with Raytracer.Materials;
with Raytracer.Vectors;
with Raytracer.Shapes;

package Raytracer.Materials.Metal is

   type C_Metal is new Raytracer.Materials.C_Material with private;
   type C_MetalPtr is access all C_Metal;

   function Make_Metal (albedo : in XC_Vector3F) return C_Metal;
   function Make_MetalPtr (albedo : in XC_Vector3F) return C_MaterialPtr;

   overriding function Scatter (metal         : in C_Metal;
                                ray_in        : in C_Ray;
                                hit_record    : in Raytracer.Shapes.XC_Hit_Record;
                                attenuation   : out XC_Vector3F;
                                ray_scattered : out C_Ray) return Boolean;

private
   type C_Metal is new Raytracer.Materials.C_Material with record
      Albedo : XC_Vector3F;
   end record;

end Raytracer.Materials.Metal;
