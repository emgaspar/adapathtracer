with Raytracer.Random_Generator; use Raytracer.Random_Generator;

package body Raytracer.Materials is

   -- Additional stuff
   function Random_In_Unit_Sphere return XC_Vector3F is
      p : XC_Vector3F;
   begin
      loop
         p := 2.0 * (Get_Random, Get_Random, Get_Random) - Identity;
         exit when Dot_Product (p, p) < 1.0;
      end loop;
      return p;
   end Random_In_Unit_Sphere;

end Raytracer.Materials;
