with Raytracer.Rays; use Raytracer.Rays;
with Raytracer.Vectors; use Raytracer.Vectors;
limited with Raytracer.Shapes;

-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
package Raytracer.Materials is

   ----------------------------- TIPOS ----------------------------------

   type C_Material is interface;
   type C_MaterialPtr is access all C_Material'Class;

   ------------------------- OPERACIONES --------------------------------

   function Scatter (material      : in C_Material;
                     ray_in        : in C_Ray;
                     hit_record    : in Raytracer.Shapes.XC_Hit_Record;
                     attenuation   : out XC_Vector3F;
                     ray_scattered : out C_Ray) return Boolean is abstract;

   -- Additional stuff
   function Random_In_Unit_Sphere return XC_Vector3F;

end Raytracer.Materials;
