package body Raytracer.Random_Generator is

   function Get_Random return DT_Float is
   begin
      return DT_Float (Ada.Numerics.Float_Random.Random (rand));
   end Get_Random;

   procedure Reset is
   begin
      Ada.Numerics.Float_Random.Reset (rand);
   end Reset;

   -------------------------------------------------------------------------------------------------

end Raytracer.Random_Generator;
