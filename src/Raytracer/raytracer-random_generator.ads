with Ada.Numerics.Float_Random;

package Raytracer.Random_Generator is

   procedure Reset;

   function Get_Random return DT_Float;

private

   rand : Ada.Numerics.Float_Random.Generator;

end Raytracer.Random_Generator;
