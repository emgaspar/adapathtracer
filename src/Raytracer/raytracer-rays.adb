package body Raytracer.Rays is

   function Direction (ray : in C_Ray) return XC_Vector3F is
   begin
      return ray.direction;
   end Direction;

   -------------------------------------------------------------------------------------------------
   --
   --  Ray constructor
   --
   --  @param  origin    Point in the space where the ray has its origin.
   --  @param  direction Direction vector related to the ray.
   --
   --  @return Return a Ray object.
   --
   -------------------------------------------------------------------------------------------------
   function Make_Ray (origin, direction : in XC_Vector3F) return C_Ray is
      Ray : C_Ray;
   begin
      Ray.origin    := origin;
      Ray.direction := direction;
      return Ray;
   end Make_Ray;

   -------------------------------------------------------------------------------------------------
   --
   --  Getter for ray origin value
   --
   --  @return Return the ray origin point.
   --
   -------------------------------------------------------------------------------------------------
   function Origin (ray : in C_Ray) return XC_Vector3F is
   begin
      return ray.origin;
   end Origin;

   -------------------------------------------------------------------------------------------------
   --
   --
   --  Compute a ray point.
   --
   --  @param  t  Parametric value to compute the parametric equation
   --
   --  @return Ray point
   --
   -------------------------------------------------------------------------------------------------
   function Point_At_Parameter (ray : in C_Ray; t : in DT_Float)
                                return XC_Vector3F is
   begin
      return ray.origin + (t * ray.direction);
   end Point_At_Parameter;

end Raytracer.Rays;
