with Raytracer.Vectors; use Raytracer.Vectors;

--
--  @summary
--  Ray class
--
--  @description
--  Class to represent a ray. It defines the ray datatype and the related methods.
--
package Raytracer.Rays is

   ------------------------------ CONSTANTES ---------------------------------

   -------------------------------- TIPOS ------------------------------------

   type C_Ray is tagged private;

   ------------------------------ VARIABLES ----------------------------------

   ------------------------------ OPERACIONES --------------------------------

   -------------------------------------------------------------------------------------------------
   --
   --  Ray constructor
   --
   --  @param  origin    Point in the space where the ray has its origin.
   --  @param  direction Direction vector related to the ray.
   --
   --  @return Return a Ray object.
   --
   -------------------------------------------------------------------------------------------------
   function Make_Ray (origin, direction : in XC_Vector3F) return C_Ray;

   -------------------------------------------------------------------------------------------------
   --
   --  Getter for ray origin value
   --
   --  @return Return the ray origin point.
   --
   -------------------------------------------------------------------------------------------------
   function Origin (ray : in C_Ray) return XC_Vector3F;

   -------------------------------------------------------------------------------------------------
   --
   --  Getter for ray direction value
   --
   --  @return Return the ray direction vector.
   --
   -------------------------------------------------------------------------------------------------
   function Direction (ray : in C_Ray) return XC_Vector3F;

   -------------------------------------------------------------------------------------------------
   --
   --  Compute a ray point.
   --
   --  @param  t  Parametric value to compute the parametric equation
   --
   --  @return Ray point
   --
   -------------------------------------------------------------------------------------------------
   function Point_At_Parameter (ray : in C_Ray; t : in DT_Float)
                                return XC_Vector3F;

   -------------------------------- TAREAS -----------------------------------

   ------------------------------ EXCEPCIONES --------------------------------

private

   -------------------------------- TIPOS ------------------------------------

   ------------------------------ CONSTANTES ---------------------------------

   -------------------------------------------------------------------------------------------------
   --
   --  A class to represent a Ray
   --
   -- @field origin     Origin of the ray.
   -- @field direction  Directional vector related to the ray
   --
   -------------------------------------------------------------------------------------------------
   type C_Ray is tagged record
      origin    : XC_Vector3F;
      direction : XC_Vector3F;
   end record;

   ------------------------------ VARIABLES ----------------------------------

   ------------------------------ OPERACIONES --------------------------------

   -------------------------------- TAREAS -----------------------------------

   ------------------------------ EXCEPCIONES --------------------------------

end Raytracer.Rays;
