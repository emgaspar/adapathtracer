package body Raytracer.Shapes.Spheres is

   overriding
   function Hit (sphere       : in C_Sphere;
                 ray          : in C_Ray;
                 t_min, t_max : in DT_Float;
                 hit_record   : out XC_Hit_Record) return Boolean is
      oc                 : XC_Vector3F;
      a, b, c            : DT_Float;
      temp, discriminant : DT_Float;
   begin
      oc := ray.Origin - sphere.Center;
      a := Dot_Product (ray.Direction, ray.Direction);
      b := Dot_Product (oc, ray.Direction);
      c := Dot_Product (oc, oc) - (sphere.Radius * sphere.Radius);
      discriminant := (b * b) - (a * c);

      if discriminant > 0.0 then -- Hay solución
         temp := (-b - DT_Float_Elementary_Functions.Sqrt (discriminant)) / a;
         if temp < t_max and then temp > t_min then
            hit_record.t := temp;
            hit_record.point := ray.Point_At_Parameter (hit_record.t);
            hit_record.normal := (hit_record.point - sphere.Center) / sphere.Radius;
            hit_record.material := sphere.Material;
            return True;
         end if;

         temp := (-b + DT_Float_Elementary_Functions.Sqrt (discriminant)) / a;
         if temp < t_max and then temp > t_min then
            hit_record.t := temp;
            hit_record.point := ray.Point_At_Parameter (hit_record.t);
            hit_record.normal := (hit_record.point - sphere.Center) / sphere.Radius;
            hit_record.material := sphere.Material;
            return True;
         end if;
      end if;

      return False;
   end Hit;

   -------------------------------------------------------------------------------------------------

   function Make_Sphere (center   : in XC_Vector3F;
                         radius   : in DT_Float;
                         material : in C_MaterialPtr) return C_Sphere is
   begin
      return (Center => center, Radius => radius, Material => material);
   end Make_Sphere;

end Raytracer.Shapes.Spheres;
