with Raytracer.Vectors; use Raytracer.Vectors;
with Raytracer.Rays; use Raytracer.Rays;
with Raytracer.Shapes; use Raytracer.Shapes;
with Raytracer.Materials; use Raytracer.Materials;

package Raytracer.Shapes.Spheres is

   type C_Sphere is new Raytracer.Shapes.C_Shape with private;

   function Make_Sphere (center   : in XC_Vector3F;
                         radius   : in DT_Float;
                         material : in C_MaterialPtr) return C_Sphere;

   overriding
   function Hit (sphere       : in C_Sphere;
                 ray          : in C_Ray;
                 t_min, t_max : in DT_Float;
                 hit_record   : out XC_Hit_Record) return Boolean;

private

   type C_Sphere is new Raytracer.Shapes.C_Shape with record
      Center   : XC_Vector3F;
      Radius   : DT_Float;
      Material : C_MaterialPtr;
   end record;

end Raytracer.Shapes.Spheres;
