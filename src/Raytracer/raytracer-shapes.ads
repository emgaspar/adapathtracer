with Raytracer.Rays; use Raytracer.Rays;
with Raytracer.Vectors; use Raytracer.Vectors;
with Raytracer.Materials; use Raytracer.Materials;

--
--  @summary
--  Shapes base class
--
--  @description
--  Shapes class definition.
--
package Raytracer.Shapes is

   ------------------------------ CONSTANTES ---------------------------------

   -------------------------------- TIPOS ------------------------------------

   -------------------------------------------------------------------------------------------------
   --
   --  A point representing a location in integer precision.
   --  @field t
   --  @field point
   --  @field normal
   --  @field material
   --
   -------------------------------------------------------------------------------------------------
   type XC_Hit_Record is record
      t        : DT_Float;
      point    : XC_Vector3F;
      normal   : XC_Vector3F;
      material : C_MaterialPtr;
   end record;

   type C_Shape is interface;

   ------------------------------ VARIABLES ----------------------------------

   ------------------------------ OPERACIONES --------------------------------

   -------------------------------------------------------------------------------------------------
   --
   -- Method to check if a ray hits on a shape.
   --
   -- @param shape      Shape to check
   -- @param ray        Ray to check if it hits on the shape.
   -- @param t_min      t minimum
   -- @param t_max      t maximum
   -- @param hit_record Data about the hit
   --
   -- @return True iff ray hits on shape.
   --
   -------------------------------------------------------------------------------------------------
   function Hit (shape       : in C_Shape;
                 ray         : in C_Ray;
                 t_min       : in DT_Float;
                 t_max       : in DT_Float;
                 hit_record  : out XC_Hit_Record) return Boolean is abstract;

   -------------------------------- TAREAS -----------------------------------

   ------------------------------ EXCEPCIONES --------------------------------

end Raytracer.Shapes;
