with Raytracer.Generic_Float_Vectors;

package Raytracer.Vectors is

   package Vectors2 is
     new Raytracer.Generic_Float_Vectors (DT_Index_Type   => DT_Index_2D,
                                          DT_Element_Type => DT_Float);
   package Vectors3 is
     new Raytracer.Generic_Float_Vectors (DT_Index_Type   => DT_Index_3D,
                                          DT_Element_Type => DT_Float);
   package Vectors4 is
     new Raytracer.Generic_Float_Vectors (DT_Index_Type   => DT_Index_4D,
                                          DT_Element_Type => DT_Float);

   type XC_Vector2F is new Vectors2.XC_Vector;
   type XC_Vector3F is new Vectors3.XC_Vector;
   type XC_Vector4F is new Vectors4.XC_Vector;

   type XC_Array_Vector2F is array (Natural range <>) of XC_Vector2F;
   type XC_Array_Vector3F is array (Natural range <>) of XC_Vector3F;
   type XC_Array_Vector4F is array (Natural range <>) of XC_Vector4F;

end Raytracer.Vectors;
