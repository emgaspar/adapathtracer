with Ada.Numerics.Generic_Elementary_Functions;
use Ada.Numerics;

package Raytracer is
   type DT_Float is new Float;

   package DT_Float_Elementary_Functions is
     new Ada.Numerics.Generic_Elementary_Functions (DT_Float);
   use DT_Float_Elementary_Functions;

   type DT_Homogeneous_Index is (X, Y, Z, W);
   subtype DT_Index_2D is DT_Homogeneous_Index range X .. Y;
   subtype DT_Index_3D is DT_Homogeneous_Index range X .. Z;
   subtype DT_Index_4D is DT_Homogeneous_Index;

end Raytracer;
