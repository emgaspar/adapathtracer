with Ada.Text_IO;  use Ada.Text_IO;
with Ada.Text_IO.Text_Streams; use Ada.Text_IO.Text_Streams;
with Ada.Numerics.Float_Random; use Ada.Numerics.Float_Random;
with Ada.Execution_Time; use Ada.Execution_Time;
with Ada.Real_Time; use Ada.Real_Time;

with Raytracer; use Raytracer;
with Raytracer.Vectors; use Raytracer.Vectors;
with Raytracer.Rays; use Raytracer.Rays;
with Raytracer.Shapes; use Raytracer.Shapes;
with Raytracer.Shapes.Spheres; use Raytracer.Shapes.Spheres;
with Raytracer.Materials; use Raytracer.Materials;
with Raytracer.Materials.Lambertian; use Raytracer.Materials.Lambertian;
with Raytracer.Materials.Metal; use Raytracer.Materials.Metal;
with Raytracer.Materials.Dielectric; use Raytracer.Materials.Dielectric;
with Raytracer.Camera; use Raytracer.Camera;
with Raytracer.Random_Generator;

procedure Main is

   --  Constante
   CT_WIDTH                       : constant Integer := 512;
   CT_HEIGHT                      : constant Integer := 256;
   CT_NUMBER_OF_SAMPLES_PER_PIXEL : constant Integer := 100;
   CT_NUMBER_OF_SHAPES            : constant Integer := 5;

   type XC_Sphere_List is array (1 .. CT_NUMBER_OF_SHAPES) of C_Sphere;

   -------------------------------------------------------------------------------------------------

   function Compute_Hit (sphere_list  : in XC_Sphere_List;
                         ray          : in C_Ray;
                         t_min, t_max : in DT_Float;
                         hit_record   : out XC_Hit_Record) return Boolean;

   -------------------------------------------------------------------------------------------------

   function Compute_Color (sphere_list : in XC_Sphere_List;
                           current_ray : in C_Ray;
                           depth       : in Integer) return XC_Vector3F;

   -------------------------------------------------------------------------------------------------

   function Compute_Color (sphere_list : in XC_Sphere_List;
                           current_ray : in C_Ray;
                           depth       : in Integer) return XC_Vector3F is
      unit_direction : XC_Vector3F;
      t              : DT_Float;
      color          : constant XC_Vector3F := (0.7, 0.5, 1.0);
      hit_record     : XC_Hit_Record;
      scattered      : C_Ray;
      attenuation    : XC_Vector3F;

   begin
      --  Compute if the ray hits the sphere (t>0.0)

      if Compute_Hit (sphere_list, current_ray, 0.001,
                      DT_Float'Last, hit_record) = True
      then

         if (depth < 50) and then
           (hit_record.material.Scatter (ray_in => current_ray,
                                         hit_record => hit_record,
                                         attenuation => attenuation,
                                         ray_scattered => scattered) = True)
         then
            return attenuation * Compute_Color (sphere_list, scattered, depth + 1);
         else
            return (0.0, 0.0, 0.0);
         end if;
      else
         unit_direction := Normalize (current_ray.Direction);
         t := 0.5 * (unit_direction (Y) + 1.0);
         return (1.0 - t) * Identity + t * color;
      end if;
   end Compute_Color;

   -------------------------------------------------------------------------------------------------

   function Compute_Hit (sphere_list  : in XC_Sphere_List;
                         ray          : in C_Ray;
                         t_min, t_max : in DT_Float;
                         hit_record   : out XC_Hit_Record) return Boolean is
      temp_hit_record : XC_Hit_Record;
      hit_anything    : Boolean := False;
      closest_so_far  : DT_Float := t_max;

   begin

      for i in 1 .. CT_NUMBER_OF_SHAPES loop
         if sphere_list (i).Hit (ray, t_min, closest_so_far, temp_hit_record) = True then
            hit_anything := True;
            closest_so_far := temp_hit_record.t;
            hit_record := temp_hit_record;
         end if;
      end loop;
      return hit_anything;
   end Compute_Hit;

   -------------------------------------------------------------------------------------------------

   ppm_file       : File_Type;
   color          : XC_Vector3F;
   ir, ig, ib     : Integer;
   u, v           : DT_Float;
   my_ray         : C_Ray;
   sphere_list    : constant XC_Sphere_List :=
     (Make_Sphere ((0.0, 0.0, -1.0), 0.5, Make_LambertianPtr ((0.8, 0.3, 0.3))),
      Make_Sphere ((0.0, -100.5, -1.0), 100.0, Make_LambertianPtr ((0.8, 0.8, 0.0))),
      Make_Sphere ((1.0, 0.0, -1.0), 0.5, Make_MetalPtr ((0.8, 0.6, 0.2))),
      Make_Sphere ((-1.0, 0.0, -1.0), 0.5, Make_DielectricPtr (1.5)),
      Make_Sphere ((-1.0, 0.0, -1.0), -0.45, Make_DielectricPtr (1.5)));

   camera         : constant C_Camera := Raytracer.Camera.Create;
   point          : XC_Vector3F;
   cpu_time_init  : CPU_Time;
   total_cpu_time : Time_Span := Time_Span_Zero;
   total_time     : Duration;

begin

   Create (File => ppm_file, Mode => Out_File, Name => "image.ppm");

   Put_Line (ppm_file, "P3");
   Put_Line (ppm_file, Integer'Image (CT_WIDTH) & " " & Integer'Image (CT_HEIGHT));
   Put_Line (ppm_file, "255");

   Random_Generator.Reset;

   for j in reverse 0 .. CT_HEIGHT - 1 loop
      for i in 0 .. CT_WIDTH - 1 loop
         cpu_time_init := Clock;

         color := Zeros;

         for s in 0 .. CT_NUMBER_OF_SAMPLES_PER_PIXEL - 1 loop
            u := (DT_Float (i) + Random_Generator.Get_Random) / DT_Float (CT_WIDTH);
            v := (DT_Float (j) + Random_Generator.Get_Random) / DT_Float (CT_HEIGHT);
            my_ray := camera.Get_Ray (u, v);
            point := my_ray.Point_At_Parameter (2.0);
            color := color + Compute_Color (current_ray => my_ray,
                                            sphere_list => sphere_list,
                                            depth => 0);
         end loop;

         -- Protecciones para evitar desbordamientos.
         if not color (X)'Valid then
            color (X) := 0.0;
         end if;

         if not color (Y)'Valid then
            color (Y) := 0.0;
         end if;

         if not color (Z)'Valid then
            color (Z) := 0.0;
         end if;

         color := color / DT_Float (CT_NUMBER_OF_SAMPLES_PER_PIXEL);
         color := (X => DT_Float_Elementary_Functions.Sqrt (color (X)),
                   Y => DT_Float_Elementary_Functions.Sqrt (color (Y)),
                   Z => DT_Float_Elementary_Functions.Sqrt (color (Z)));

         ir := Integer (255.0 * color (X));
         ig := Integer (255.0 * color (Y));
         ib := Integer (255.0 * color (Z));

         total_cpu_time := total_cpu_time + (Clock - cpu_time_init);
         Put_Line (ppm_file, Integer'Image (ir) & " " & Integer'Image (ig) &
                     " " & Integer'Image (ib));
      end loop;
   end loop;
   total_time := To_Duration (total_cpu_time);
   Put_Line ("CPU Time = " & Duration'Image (total_time) & " - " &
              DT_Float'Image
                (DT_Float (CT_WIDTH * CT_HEIGHT * CT_NUMBER_OF_SAMPLES_PER_PIXEL) /
                  (DT_Float (total_time) * 1.0e6)) & " MRay/s");

   Close (ppm_file);

end Main;
